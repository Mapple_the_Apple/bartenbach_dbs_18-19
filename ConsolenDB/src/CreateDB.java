
import java.sql.*;
import java.util.Scanner;

public class CreateDB {
	private Connection connection;
	private String dbname;
	private String user = "root";
	private String pwd;
	
	static { 
        try { 
            Class<?> c = Class.forName("com.mysql.jdbc.Driver"); 
            if (c != null) { 
                System.out.println("JDBC-Treiber geladen"); 
            } 
        } catch (ClassNotFoundException e) { 
            System.err.println("Fehler beim Laden des JDBC-Treibers"); 
            System.exit(1); 
        } 
    } 
	
	public CreateDB(String dbname, String pwd){
		this.dbname=dbname;
		this.pwd=pwd;
	}
	public CreateDB() throws SQLException { 
        createConnection(); 
        createDBStructure(); 
        Thread shutDownHook = new Thread() { 
            public void run() { 
                System.out.println("Running shutdown hook"); 
                if(connection == null) System.out.println("Connedtion to database already closed"); 
                try { 
                    if (connection != null && !connection.isClosed()) { 
                        connection.close(); 
                        if (connection.isClosed()) 
                            System.out.println("Connection to database closed"); 
                    } 
                } catch (SQLException e) { 
                    System.err.println( 
                            "Shutdown hook couldn't close database connection."); 
                } 
            } 
        }; 
        Runtime.getRuntime().addShutdownHook(shutDownHook); 
    } 
	
	private Connection createConnection() throws SQLException { 
		
		String conStr = "jdbc:mysql://localhost/"+dbname+ "?user=" +user+ "&password=" +pwd;
		DriverManager.getConnection(conStr);
		return DriverManager.getConnection(conStr);
    } 
	private void releaseConnection(Connection con) throws SQLException {
		con.close();
	}
	
	private boolean createDBStructure() { 
        String dbName = "TemperaturDB"; 

        String query0 = "CREATE DATABASE IF NOT EXISTS `" + dbName + "`"; 

        String query1 = "USE `" + dbName + "`"; 

        String query2 = "SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO'; "; 

        String query3 = "CREATE TABLE IF NOT EXISTS `Personen` (" 
                + "Vorname VARCHAR(20) NOT NULL, " 
                + "Nachname VARCHAR(20) NOT NULL,"
                + "Geburtstag INTEGER,"
                + "Geburtsmonat VARCHAR(20),"
                + "Geburtsjahr INTEGER);";
        
               

        Statement stmt = null; 
        try { 
            connection.setAutoCommit(false); 
            stmt = connection.createStatement(); 
            stmt.addBatch(query0); 
            stmt.addBatch(query1); 
            stmt.addBatch(query2); 
            stmt.addBatch(query3);
            stmt.executeBatch(); 
            connection.commit(); 
            stmt.close(); 
            connection.close(); 
            System.out.println("Database successfully created or just existing"); 
            return true;
            
        } catch (SQLException e) { 
            e.printStackTrace(); 
        } finally { 
            try { 
                if (stmt != null) 
                    stmt.close(); 
                if (connection != null) 
                    connection.close(); 
            } catch (SQLException e) { 
            } 
        } 
        return false; 
    }
	
	public void insert(String vorname, String nachname, int geburtstag, String geburtsmonat, int geburtsjahr) throws SQLException {
		Connection con=null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		try {
			con = createConnection();
			
			
			String query="INSERT INTO benutzer (Vorname, Nachname, Geburtstag, Geburtsmonat, Geburtsjahr) VALUES (?,?,?,?,?)";
			
			stm = con.prepareStatement(query);
			stm.setString(1, vorname);
			stm.setString(2, nachname);
			stm.setInt(3, geburtstag);
			stm.setString(4, geburtsmonat);
			stm.setInt(5, geburtsjahr);
			
			
		}
		
		finally {
			
			if(stm !=null)
					stm.close();
			if(con != null) 
				releaseConnection(con);
		}
	}
	
	
	
	public static void main(String[] args) throws SQLException {
		Scanner sc = new Scanner(System.in);
		String dbname;
		String pwd;
		String vorname;
		String nachname;
		int geburtstag;
		String geburtsmonat;
		int geburtsjahr;
		
		System.out.print("Bitte geben Sie den zu erstellenden Datenbanknamen ein: ");
		dbname=sc.next();
		System.out.println();
		
		System.out.print("Passwort: ");
		pwd=sc.next();
		System.out.println();
		
		new CreateDB(dbname,pwd);
		
		CreateDB db = new CreateDB(); 
		
		System.out.print("Vorname: ");
		vorname=sc.next();
		System.out.println();
		
		System.out.print("Nachname: ");
		nachname=sc.next();
		System.out.println();
		
		System.out.print("Geburtstag (z.B.: 01): ");
		geburtstag=sc.nextInt();
		System.out.println();
		
		System.out.print("Geburtsmonat (z.B.: Juni): ");
		geburtsmonat=sc.next();
		System.out.println();
		
		System.out.print("Geburtsjahr (z.B.: 2000): ");
		geburtsjahr=sc.nextInt();
		System.out.println();
		
		db.insert(vorname, nachname, geburtstag, geburtsmonat, geburtsjahr);
		
	}

}
