import java.awt.Dimension;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

public class DBManager {

	private static Connection c;
	private String pw = "bartenbach";
	private String databaseName = "aircraft";
	private Scanner sc = new Scanner(System.in);

	DBManager() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost/" + databaseName, "root", pw);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Integer> getTimeStamp() throws SQLException {
		String sql = "SELECT seentime FROM dump1090data";
		PreparedStatement stmt = null;
		ArrayList<Integer> list = new ArrayList<Integer>();

		Date date = new Date();
		System.out.println(date.getTime() - 31536000);
		try {
			stmt = c.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getInt(1) > date.getTime() / 1000 - 31536000) {
					list.add(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return list;

		// Date date3 =

	}

	public void plotFlights() throws SQLException {
		int[] arr = new int[12];
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		for (int element : getTimeStamp()) {
			Date date = Date.from(Instant.ofEpochSecond(element));
			String month = sdf.format(date);
			switch (month) {
			case "01":
				arr[0]++;
				break;
			case "02":
				arr[1]++;
				break;
			case "03":
				arr[2]++;
				break;
			case "04":
				arr[3]++;
				break;
			case "05":
				arr[4]++;
				break;
			case "06":
				arr[5]++;
				break;
			case "07":
				arr[6]++;
				break;
			case "08":
				arr[7]++;
				break;
			case "09":
				arr[8]++;
				break;
			case "10":
				arr[9]++;
				break;
			case "11":
				arr[10]++;
				break;
			case "12":
				arr[11]++;
				break;
			}
		}
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(arr[11], "1.782 Flüge", "Dezember");
		dataset.addValue(arr[0], "123.707 Flüge", "Jänner");
		dataset.addValue(arr[1], "103.009 Flüge", "Februar");
		dataset.addValue(arr[2], "28.250 Flüge", "März");
		dataset.addValue(arr[3], "keine Daten", "April");
		dataset.addValue(arr[4], "96.678 Flüge", "Mai");
		dataset.addValue(arr[5], "97.392 Flüge", "Juni");
		dataset.addValue(arr[6], "78.600 Flüge", "Juli");
		dataset.addValue(arr[7], "66.806 Flüge", "August");
		dataset.addValue(arr[8], "43.884 Flüge", "September");
		dataset.addValue(arr[9], "193.737 Flüge", "Oktober");
		dataset.addValue(arr[10], "keine Daten", "November");
		

		JFreeChart chart = ChartFactory.createBarChart("Flüge in letzten 12 Monaten", // chart title
				"Monate", // domain axis label
				"Anzahl der Flüge", // range axis label
				dataset, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips
				false // URLs
		);
		
		ChartPanel chartPanel = new ChartPanel(chart, false);
		chartPanel.setPreferredSize(new Dimension(1000, 600));

		ApplicationFrame punkteframe = new ApplicationFrame("Flüge");

		punkteframe.setContentPane(chartPanel);
		punkteframe.pack();
		punkteframe.setVisible(true);

	}
}
