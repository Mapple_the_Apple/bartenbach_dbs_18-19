import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Temperature {

	private int temID;
	private int temperature;
	private Date dateTem = new Date();
	private String dateStr;
	private Random rnd = new Random();
	private SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
	
	
	public Temperature(int temperature, String dateStr) {
		this.temperature = temperature;
		this.dateStr = dateStr;
	}
	
	public Temperature(int prevTem) {
		genTem(prevTem);
		dateStr = sdf.format(dateTem);
	}
	
	public void genTem(int prevTem) {
		temperature = prevTem + rnd.nextInt(3) - 1;
	}

	public int getTemID() {
		return temID;
	}

	public void setTemID(int temID) {
		this.temID = temID;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public Date getDateTem() {
		return dateTem;
	}

	public void setDateTem(Date dateTem) {
		this.dateTem = dateTem;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	
	
	
	
}
