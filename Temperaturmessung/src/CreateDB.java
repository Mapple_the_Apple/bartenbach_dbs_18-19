import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class CreateDB { 

    private Connection connection;
    private double temperatur = 20.0;

    static { 
        try { 
            Class<?> c = Class.forName("com.mysql.jdbc.Driver"); 
            if (c != null) { 
                System.out.println("JDBC-Treiber geladen"); 
            } 
        } catch (ClassNotFoundException e) { 
            System.err.println("Fehler beim Laden des JDBC-Treibers"); 
            System.exit(1); 
        } 
    } 

    public CreateDB() { 
        createConnection(); 
        createDBStructure(); 
        Thread shutDownHook = new Thread() { 
            public void run() { 
                System.out.println("Running shutdown hook"); 
                if(connection == null) System.out.println("Connedtion to database already closed"); 
                try { 
                    if (connection != null && !connection.isClosed()) { 
                        connection.close(); 
                        if (connection.isClosed()) 
                            System.out.println("Connection to database closed"); 
                    } 
                } catch (SQLException e) { 
                    System.err.println( 
                            "Shutdown hook couldn't close database connection."); 
                } 
            } 
        }; 
        Runtime.getRuntime().addShutdownHook(shutDownHook); 
    } 

    private void createConnection() { 
        String url = "jdbc:mysql://localhost/?rewriteBatchedStatements=true"; 
        String user = "root"; 
        String pass = "Starwars12"; 
        try { 
            System.out.println("Creating DBConnection"); 
            connection = DriverManager.getConnection(url, user, pass); 
        } catch (SQLException e) { 
            System.err.println("Couldn't create DBConnection"); 
            System.exit(1); 
        } 
    } 

    private boolean createDBStructure() { 
        String dbName = "TemperaturDB"; 

        String query0 = "CREATE DATABASE IF NOT EXISTS `" + dbName + "`"; 

        String query1 = "USE `" + dbName + "`"; 

        String query2 = "SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO'; "; 

        String query3 = "CREATE TABLE IF NOT EXISTS `Temperaturen` (" 
                + "`Tag` NOT NULL DEFAULT AUTOINCREMENT, " 
                + "`Temperatur` INTEGER NOT NULL)";
        
        
		String query4 = "INSERT INTO "+ dbName + " (Tag, Temperatur)"
        		+ " VALUES (1"  + neueTemperatur() + ");";
		String query5 = "INSERT INTO "+ dbName + " (Temperatur)"
        		+ " VALUES (" + neueTemperatur() + ");";
               

        Statement stmt = null; 
        try { 
            connection.setAutoCommit(false); 
            stmt = connection.createStatement(); 
            stmt.addBatch(query0); 
            stmt.addBatch(query1); 
            stmt.addBatch(query2); 
            stmt.addBatch(query3); 
            stmt.executeBatch(); 
            connection.commit(); 
            stmt.close(); 
            connection.close(); 
            System.out.println("Database successfully created or just existing"); 
            return true;
            
        } catch (SQLException e) { 
            e.printStackTrace(); 
        } finally { 
            try { 
                if (stmt != null) 
                    stmt.close(); 
                if (connection != null) 
                    connection.close(); 
            } catch (SQLException e) { 
            } 
        } 
        return false; 
    } 
    
    public double neueTemperatur(){
    	Random fall = new Random();
    	Random zufallstemperatur = new Random();
    	int i = fall.nextInt(2);
    	
    	if(i==1) this.temperatur = this.temperatur + zufallstemperatur.nextDouble();
    	if(i==2) this.temperatur = this.temperatur - zufallstemperatur.nextDouble();
    	
    	return(temperatur);
    }

    public static void main(String[] args) { 
        new CreateDB(); 
    } 
} 